export class Driver {
  public id: string;
  public fullName: string;
  public phoneNumber: string;
  public carColor: string;
  public carNumber: string;
  public available: boolean;
  public driverImage: string;
  public carImage: string;
  public carModel: string;
}

export interface IDriverImageResponse {
  destination: string;
  encoding: string;
  fieldname: string;
  filename: string;
  mimetype: string;
  originalname: string;
  path: string;
  size: number;
  type: string;
}
