import { User } from './user';

export enum OrderStatus {
  Pending = 'Pending',
  Accepted = 'Accepted',
  Canceled = 'Canceled',
  Completed = 'Completed',
  Arrived = 'Arrived',
}

export class TaxiOrder {
  public id: string;
  public longitudeFrom: number;
  public latitudeFrom: number;
  public longitudeTo: number;
  public latitudeTo: number;
  public addressFrom: string;
  public addressTo: string;
  public price: number;
  public status: OrderStatus;
  public userId: string;
  public user: User;
}

export class TaxiUserOrder {
  public taxiOrder: TaxiOrder;
  public user: User;
  public clientId: string;
  public duration: number;
}
