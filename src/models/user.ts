import { IJwtPaylod } from '../providers/auth/auth';
import { Driver } from './driver';
import { Client } from './client';

export class User implements IJwtPaylod {
  public accessToken: string;
  public expiresIn: number;
  public id: string;
  public name: string;
  public username: string;
  public password: string;
  public type: string;
  public driver?: Driver;
  public client?: Client;
}
