import { NgModule } from '@angular/core';
import { AutocompleteComponent } from './autocomplete/autocomplete';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';
import { MustMatchDirective } from '../helpers/password/mustmatch.directive';
import { DriverRegisterFormComponent } from '../pages/register/driver-register-form/driver-register-form';
import { ClientRegisterFormComponent } from '../pages/register/client-register-form/client-register-form';
import { DriverHeaderComponent } from '../pages/driver/driver-header/driver-header';
import { DriverFabButtonComponent } from '../pages/driver/driver-fab-button/driver-fab-button';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations: [
    AutocompleteComponent,
    DriverRegisterFormComponent,
    MustMatchDirective,
    ClientRegisterFormComponent,
    DriverHeaderComponent,
    DriverFabButtonComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule,
  ],
  exports: [
    AutocompleteComponent,
    DriverRegisterFormComponent,
    ClientRegisterFormComponent,
    DriverHeaderComponent,
    DriverFabButtonComponent,
  ],
})
export class ComponentsModule {}
