import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { MapsAPILoader } from '@agm/core';

/**
 * Generated class for the AutocompleteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'autocomplete',
  templateUrl: 'autocomplete.html',
  styles: ['autocomplete.scss'],
})
export class AutocompleteComponent implements AfterViewInit {
  @ViewChild('autocomplete') public autocompleteInput: ElementRef;

  @Output() public setAddress: EventEmitter<any> = new EventEmitter();
  @Output() public streetChange: EventEmitter<string> = new EventEmitter<
    string
  >();

  @Input() public street: string;
  @Input() public placeholder: string;

  constructor(private mapsAPILoader: MapsAPILoader) {}

  public ngAfterViewInit(): void {
    this.mapsAPILoader.load().then(() => {
      this.getPlaceAutocomplete();
    });
  }

  public invokeEvent(place: object): void {
    this.setAddress.emit(place);
  }

  public streetChangeEmitter(): void {
    this.streetChange.emit(this.street);
  }

  private getPlaceAutocomplete(): void {
    const autocomplete = new google.maps.places.Autocomplete(
      this.autocompleteInput.nativeElement,
      {
        componentRestrictions: { country: 'BG' },
        types: ['geocode', 'establishment'],
      }
    );
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);
    });
  }
}
