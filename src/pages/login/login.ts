import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { User } from '../../models/user';
import { AuthProvider } from '../../providers/auth/auth';
import { finalize } from 'rxjs/operators';
import { Storage } from '@ionic/storage';
import { AlertProvider } from '../../providers/alert/alert';
import { AppConstant } from '../../app/constants/app.constants';
import { DriverProvider } from '../../providers/driver/driver';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public user: User;

  constructor(
    private readonly authService: AuthProvider,
    private readonly alertService: AlertProvider,
    private readonly driverService: DriverProvider,
    private readonly storage: Storage
  ) {
    this.user = new User();
  }

  public login(): void {
    this.alertService.showLoading('Логване...', 'bubbles');

    this.authService
      .login(this.user)
      .pipe(finalize(() => this.alertService.hideLoading()))
      .subscribe(data => {
        if (data) {
          this.authService.userObserver.next(data.id);
          this.storage.set(AppConstant.STORAGE_USER_ID, data.id);

          if (data.driver && Object.keys(data.driver).length > 0) {
            this.driverService.driverObserver.next(data.driver);
            this.storage.set(AppConstant.STORAGE_USER_DRIVER, data.driver);
          }
        }
      });
  }
}
