import { Component, OnInit } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { ChatProvider, Message } from '../../providers/chat/chat';
import { PusherProvider } from '../../providers/pusher/pusher';
import { AuthProvider } from '../../providers/auth/auth';
import { Driver } from '../../models/driver';
import { Client } from '../../models/client';
import { DriverProvider } from '../../providers/driver/driver';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage implements OnInit {
  public message: Message = new Message();
  public messages: Message[] = [];
  public user: any;
  public username: string;
  public now: Date;
  private channel: any;

  constructor(
    public viewCtrl: ViewController,
    private chatService: ChatProvider,
    private pusher: PusherProvider,
    private params: NavParams,
    private userService: AuthProvider
  ) {}

  public ngOnInit(): void {
    this.channel = this.pusher.init();
    this.user = this.params.get('user');

    if (this.user.user.type === 'client') {
      this.username = 'driver';
    } else {
      this.username = 'darko';
    }

    this.channel.bind('taxi_message', (data: any) => {
      this.messages.push(data);
      this.now = new Date();
    });
  }

  public dismiss(): void {
    this.viewCtrl.dismiss();
  }

  public sendMessage(message: Message): void {
    message.from = this.userService.userId;
    this.chatService.sendMessage(message).subscribe(() => {
      this.message.text = '';
      this.now = new Date();
    });
  }
}
