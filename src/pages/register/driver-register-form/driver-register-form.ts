import { Component, Output, EventEmitter } from '@angular/core';
import { User } from '../../../models/user';
import { Driver, IDriverImageResponse } from '../../../models/driver';
import { ActionSheetController, Platform } from 'ionic-angular';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera';
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject,
} from '@ionic-native/file-transfer';
import { SERVER_URL } from '../../../config/config';
import { AlertProvider } from '../../../providers/alert/alert';

@Component({
  selector: 'driver-register-form',
  templateUrl: 'driver-register-form.html',
  styles: ['driver-register-form.scss'],
})
export class DriverRegisterFormComponent {
  @Output() public registerSubmit: EventEmitter<User> = new EventEmitter<
    User
  >();

  public user: User;
  public confirmPassword: string;

  constructor(
    private actionSheetController: ActionSheetController,
    private camera: Camera,
    private platform: Platform,
    private transfer: FileTransfer,
    private alertService: AlertProvider
  ) {
    this.user = new User();
    this.user.driver = new Driver();
  }

  public registerUser(user: User): void {
    this.registerSubmit.emit(user);
  }

  public async selectImage(type: string) {
    const actionSheet = await this.actionSheetController.create({
      title: 'Изберете снимка',
      buttons: [
        {
          text: 'Галерия',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY, type);
          },
        },
        {
          text: 'Камера',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA, type);
          },
        },
        {
          text: 'Cancel',
          role: 'cancel',
        },
      ],
    });
    await actionSheet.present();
  }

  private takePicture(sourceType: PictureSourceType, type: string) {
    const options: CameraOptions = {
      quality: 100,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
    };

    const cameraOptions: FileUploadOptions = {
      chunkedMode: false,
      fileName: 'image_' + new Date().getTime() + '.jpeg',
      mimeType: 'image/jpeg',
      httpMethod: 'post',
      params: { type },
    };
    const fileTransfer: FileTransferObject = this.transfer.create();

    this.camera.getPicture(options).then(async imagePath => {
      if (this.platform.is('android')) {
        if (imagePath) {
          const response = await this.uploadImage(
            fileTransfer,
            imagePath,
            cameraOptions
          );
          if (response.responseCode === 200) {
            const res: IDriverImageResponse = JSON.parse(
              response.response
            ) as IDriverImageResponse;
            this.alertService.presentAlert(
              'Снимката е качена успешно!',
              'Успешно'
            );
            if (res.type === 'carImage') {
              this.user.driver.carImage = res.path;
            } else {
              this.user.driver.driverImage = res.path;
            }
          }
        }
      }
    });
  }

  private async uploadImage(
    fileTransfer: FileTransferObject,
    imageData: any,
    options
  ): Promise<any> {
    return await fileTransfer.upload(
      imageData,
      `${SERVER_URL}/driver/image`,
      options
    );
  }
}
