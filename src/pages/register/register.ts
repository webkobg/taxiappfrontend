import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IonicPage } from 'ionic-angular';
import { User } from '../../models/user';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public user: User;
  public confirmPassword: string;
  public segment: string = 'client';

  constructor(private userService: AuthProvider, private router: Router) {
    this.user = new User();
  }

  public registerUser(user: User): void {
    user.type = this.segment;
    this.userService.register(user).subscribe(response => {
      if (response) {
        this.router.navigateByUrl('/login');
      }
    });
  }

  public goBack(): void {
    this.router.navigateByUrl('/login');
  }
}
