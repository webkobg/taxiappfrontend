import { Component, Output, EventEmitter } from '@angular/core';
import { User } from '../../../models/user';
import { Client } from '../../../models/client';

@Component({
  selector: 'client-register-form',
  templateUrl: 'client-register-form.html',
  styles: ['client-register-form.scss'],
})
export class ClientRegisterFormComponent {
  @Output() public registerSubmit: EventEmitter<User> = new EventEmitter<
    User
  >();

  public user: User;
  public confirmPassword: string;

  constructor() {
    this.user = new User();
    this.user.client = new Client();
  }

  public registerUser(user: User): void {
    this.registerSubmit.emit(user);
  }
}
