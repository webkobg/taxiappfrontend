/// <reference types="@types/googlemaps" />
import {
  Component,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform, ModalController } from 'ionic-angular';
import { LocationTrackerProvider } from '../../../providers/location-tracker/location-tracker';
import { Subject, Observable } from 'rxjs';
import { takeUntil, finalize } from 'rxjs/operators';
import {
  TaxiOrder,
  OrderStatus,
  TaxiUserOrder,
} from '../../../models/taxiorder';
import { TaxiorderProvider } from '../../../providers/taxiorder/taxiorder';
import { PusherProvider } from '../../../providers/pusher/pusher';
import { AuthProvider } from '../../../providers/auth/auth';
import { AlertProvider } from '../../../providers/alert/alert';
import { PermissionProvider } from '../../../providers/permission/permission';
import { AppConstant } from '../../../app/constants/app.constants';
import { Router } from '@angular/router';
import { AppMessage } from '../../../app/constants/app.messages';
import { SERVER_URL } from '../../../config/config';
import { ChatPage } from '../../chat/chat';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  changeDetection: ChangeDetectionStrategy.Default,
})
export class HomePage implements OnInit, OnDestroy {
  public taxiOrder: TaxiOrder;
  public receivedTaxiOrder: TaxiUserOrder;
  public marker: google.maps.Marker;
  public firstPlace: google.maps.LatLng;
  public secondPlace: google.maps.LatLng;
  public mapStyle$: Observable<any>;
  public map: any;
  public areDirectionsVisible: boolean = false;
  public isOrderAccepted: boolean = false;
  public carImageUrl: string;
  public driverImageUrl: string;

  private destroy$: Subject<boolean> = new Subject<boolean>();
  private geocoder: google.maps.Geocoder;
  private channel: any;

  constructor(
    public geolocation: Geolocation,
    private locationService: LocationTrackerProvider,
    public platform: Platform,
    private cd: ChangeDetectorRef,
    private router: Router,
    private permissionService: PermissionProvider,
    private taxiOrderService: TaxiorderProvider,
    private pusherService: PusherProvider,
    private userService: AuthProvider,
    private alertService: AlertProvider,
    private modalController: ModalController
  ) {
    this.taxiOrder = new TaxiOrder();
    this.receivedTaxiOrder = new TaxiUserOrder();
  }

  public ngOnInit(): void {
    this.permissionService.checkGPSPermission();
    this.channel = this.pusherService.init();
    this.mapStyle$ = this.locationService.getMapStyle();

    this.channel.bind(
      AppConstant.TAXI_ORDER_PUSH_CHANNEL,
      (data: TaxiUserOrder) => {
        if (
          data.user.type === AppConstant.USER_TYPE.driver &&
          data.clientId === this.userService.userId
        ) {
          this.receivedTaxiOrder = data;
          if (data.taxiOrder.status === OrderStatus.Accepted) {
            this.carImageUrl = `${SERVER_URL}/${data.user.driver.carImage}`;
            this.driverImageUrl = `${SERVER_URL}/${data.user.driver.driverImage}`;

            this.isOrderAccepted = true;
            this.alertService.presentAlert(
              AppMessage.TAXI_ORDERED_TITLE_HOME,
              AppMessage.TAXI_ORDERED_MESSAGE_HOME +
                (data.duration / 60).toFixed() +
                ' мин.'
            );
          } else if (data.taxiOrder.status === OrderStatus.Canceled) {
            this.areDirectionsVisible = false;
            this.alertService.presentAlert(
              AppMessage.TAXI_CANCELED_TITLE_HOME,
              AppMessage.TAXI_CANCELED_MESSAGE_HOME
            );
          } else if (data.taxiOrder.status === OrderStatus.Completed) {
            this.areDirectionsVisible = false;
            this.isOrderAccepted = false;
            this.taxiOrder = new TaxiOrder();
          } else if (data.taxiOrder.status === OrderStatus.Arrived) {
            this.alertService.presentAlert(
              AppMessage.TAXI_ARRIVED_TITLE_HOME,
              AppMessage.TAXI_ARRIVED_MESSAGE_HOME
            );
          }
          this.alertService.hideLoading();
        }
      }
    );

    this.channel.bind(AppConstant.TAXI_LOCATION_PUSH_CHANNEL, (data: any) => {
      this.marker.setPosition({
        lat: data.location.coords.latitude,
        lng: data.location.coords.longitude,
      });

      this.map.setCenter({
        lat: data.location.coords.latitude,
        lng: data.location.coords.longitude,
      });
    });
  }

  public ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    this.channel.unbind();
  }

  public getCurrentLocation() {
    this.permissionService.checkGPSPermission();
    this.alertService.showLoading('Изчакайте', 'crescent');
    this.locationService
      .getCurrentLocation()
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => this.alertService.hideLoading())
      )
      .subscribe(async (data: Position) => {
        this.taxiOrder.latitudeFrom = data.coords.latitude;
        this.taxiOrder.longitudeFrom = data.coords.longitude;

        const geocode = await this.locationService.geocodeLatLng(
          this.geocoder,
          this.taxiOrder.latitudeFrom,
          this.taxiOrder.longitudeFrom
        );

        this.taxiOrder.addressFrom = geocode.addressFrom;
        this.firstPlace = geocode.firstPlace;
      });
  }

  public mapReady(event: any): void {
    this.map = event;
    this.map.setCenter({ lat: 42.698334, lng: 23.319941 });
    this.marker = new google.maps.Marker({
      map: event,
      icon: {
        url: AppConstant.DRIVER_MARKER_ICON,
        scaledSize: new google.maps.Size(40, 40),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 0),
      },
    });
    this.geocoder = new google.maps.Geocoder();
    event.controls[google.maps.ControlPosition.TOP_RIGHT].push(
      document.getElementById('Settings')
    );
  }

  public getAddressFrom(place: google.maps.places.PlaceResult): void {
    this.firstPlace = new google.maps.LatLng(
      place.geometry.location.lat(),
      place.geometry.location.lng()
    );
    this.taxiOrder.latitudeFrom = place.geometry.location.lat();
    this.taxiOrder.longitudeFrom = place.geometry.location.lng();

    this.taxiOrder.addressFrom = place.formatted_address;
  }

  public getAddressTo(place: google.maps.places.PlaceResult): void {
    this.secondPlace = new google.maps.LatLng(
      place.geometry.location.lat(),
      place.geometry.location.lng()
    );
    this.taxiOrder.latitudeTo = place.geometry.location.lat();
    this.taxiOrder.longitudeTo = place.geometry.location.lng();

    const distance = google.maps.geometry.spherical.computeDistanceBetween(
      this.firstPlace,
      this.secondPlace
    );
    this.taxiOrder.price = (distance / 1000) * AppConstant.PRICE_PER_KM;
    this.taxiOrder.addressTo = place.formatted_address;
    this.areDirectionsVisible = true;
    this.cd.detectChanges();
  }

  public requestTaxiOrder(): void {
    this.taxiOrderService.addTaxiOrder(this.taxiOrder).subscribe(response => {
      if (response) {
        this.alertService.showLoading(
          AppMessage.TAXI_REQUEST_LOADING_MESSAGE,
          'dots'
        );
      }
    });
  }

  public async logout(): Promise<void> {
    await this.userService.logout();
    this.router.navigateByUrl('login');
  }

  public async openModal() {
    const profileModal = this.modalController.create(ChatPage, {
      user: this.receivedTaxiOrder,
    });
    profileModal.present();

    // profileModal.onDidDismiss(() => {  };
  }
}
