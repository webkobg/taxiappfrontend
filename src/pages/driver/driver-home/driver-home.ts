/// <reference types="@types/googlemaps" />

import {
  Component,
  OnInit,
  trigger,
  transition,
  animate,
  style,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { IonicPage, ModalController } from 'ionic-angular';
import { PusherProvider } from '../../../providers/pusher/pusher';
import { TaxiUserOrder, OrderStatus } from '../../../models/taxiorder';
import { AlertProvider } from '../../../providers/alert/alert';
import { TaxiorderProvider } from '../../../providers/taxiorder/taxiorder';
import { LocationTrackerProvider } from '../../../providers/location-tracker/location-tracker';
import { Driver } from '../../../models/driver';
import { Observable } from 'rxjs';
import { DriverProvider } from '../../../providers/driver/driver';
import { AppConstant } from '../../../app/constants/app.constants';
import { ChatPage } from '../../chat/chat';

@IonicPage()
@Component({
  selector: 'page-driver-home',
  templateUrl: 'driver-home.html',
  animations: [
    trigger('enterAnimation', [
      transition(':enter', [
        style({ transform: 'translateX(100%)', opacity: 0 }),
        animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0)', opacity: 1 }),
        animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 })),
      ]),
    ]),
  ],
})
export class DriverHomePage implements OnInit, OnDestroy {
  @ViewChild('directionsPanel') public directionsPanel: ElementRef;

  public hasContinue: boolean = false;
  public isOrderAccepted: boolean = false;
  public areDirectionsVisible: boolean = false;
  public isArrived: boolean = false;

  public receivedTaxiOrder: TaxiUserOrder;
  public driver: Driver;
  public map: google.maps.Map;
  public mapStyle$: Observable<any>;
  public waypoints = [];
  public currentLocation: string = '';
  public currentLocationAddress: string = '';
  public geocoder: google.maps.Geocoder;

  public markerOptions = {
    waypoints: {
      infoWindow: `<h4>A<h4>
      <a href='http://google.com' target='_blank'>A</a>
      `,
    },
  };
  private channel: any;

  constructor(
    private pusherService: PusherProvider,
    private alertService: AlertProvider,
    private taxiOrderService: TaxiorderProvider,
    private locationService: LocationTrackerProvider,
    private driverService: DriverProvider,
    private modalController: ModalController
  ) {
    this.receivedTaxiOrder = new TaxiUserOrder();
    this.driver = new Driver();
  }

  public ngOnInit(): void {
    this.channel = this.pusherService.init();
    this.mapStyle$ = this.locationService.getMapStyle();

    this.channel.bind(
      AppConstant.TAXI_ORDER_PUSH_CHANNEL,
      async (data: TaxiUserOrder) => {
        if (
          data.user.type === AppConstant.USER_TYPE.client &&
          this.driverService.driverObserver.value.available
        ) {
          this.receivedTaxiOrder = data;
          this.areDirectionsVisible = true;

          this.waypoints = [
            {
              location: {
                lat: this.receivedTaxiOrder.taxiOrder.latitudeFrom,
                lng: this.receivedTaxiOrder.taxiOrder.longitudeFrom,
              },
              stopover: true,
            },
          ];

          const alert = this.alertService.presentAlert(
            'Искате ли да продължите с приемането на поръчката?',
            'Имате нова поръчка за каране!'
          );

          if (alert) {
            this.alertService.acceptSubject.subscribe(
              accept => (this.hasContinue = accept)
            );
          }
        }
      }
    );
  }

  public mapReady(map): void {
    this.map = map;
    this.map.setCenter({ lat: 42.698334, lng: 23.319941 });
    this.geocoder = new google.maps.Geocoder();
    this.getCurrentLocation();
  }

  // public startNavigating(addressFrom: string, addressTo: string) {
  //   const directionsService = new google.maps.DirectionsService();
  //   const directionsDisplay = new google.maps.DirectionsRenderer();
  //   // TODO check how to get work

  //   directionsDisplay.setMap(this.map);
  //   directionsDisplay.setPanel(this.directionsPanel.nativeElement);

  //   directionsService.route(
  //     {
  //       origin: addressFrom,
  //       destination: addressTo,
  //       travelMode: google.maps.TravelMode.DRIVING,
  //     },
  //     (res, status) => {
  //       if (status === google.maps.DirectionsStatus.OK) {
  //         directionsDisplay.setDirections(res);
  //       }
  //     }
  //   );
  // }

  public async openModal() {
    const profileModal = this.modalController.create(ChatPage, {
      user: this.receivedTaxiOrder,
    });
    profileModal.present();

    // profileModal.onDidDismiss(() => {  };
  }

  public getCurrentLocation(): void {
    this.locationService
      .getCurrentLocation()
      .subscribe(async (data: Position) => {
        const geocode = await this.locationService.geocodeLatLng(
          this.geocoder,
          data.coords.latitude,
          data.coords.longitude
        );

        this.currentLocation = geocode.firstPlace;
        this.currentLocationAddress = geocode.addressFrom;
      });
  }

  public updateOrderStatus(status: string): void {
    this.receivedTaxiOrder.taxiOrder.status = OrderStatus[status];
    this.taxiOrderService
      .updateTaxiOrderStatus(
        this.receivedTaxiOrder.taxiOrder,
        this.receivedTaxiOrder.user.id,
        this.currentLocationAddress
      )
      .subscribe(() => {
        this.checkOrderStatus(status);
      });
  }

  public ngOnDestroy(): void {
    this.channel.unbind();
  }

  private checkOrderStatus(status: string): void {
    switch (status) {
      case OrderStatus.Accepted:
        this.isOrderAccepted = true;
        this.locationService.startTracking(
          this.receivedTaxiOrder.taxiOrder.addressTo
        );
        break;
      case OrderStatus.Canceled:
        this.areDirectionsVisible = false;
        this.hasContinue = false;
        break;
      case OrderStatus.Completed:
        this.areDirectionsVisible = false;
        this.isOrderAccepted = false;
        this.hasContinue = false;
        this.isArrived = false;
        break;
      case OrderStatus.Arrived:
        this.isArrived = true;
        break;
    }
  }
}
