import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverHomePage } from './driver-home';
import { DriverHeaderComponent } from '../driver-header/driver-header';
import { ChatPage } from '../../chat/chat';

@NgModule({
  declarations: [DriverHomePage, DriverHeaderComponent],
  imports: [IonicPageModule.forChild(DriverHomePage)],
  entryComponents: [ChatPage],
})
export class DriverHomePageModule {}
