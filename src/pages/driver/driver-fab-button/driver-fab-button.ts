import { Component, Input } from '@angular/core';
import { AuthProvider } from '../../../providers/auth/auth';
import { Router } from '@angular/router';

/**
 * Generated class for the DriverFabButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'driver-fab-button',
  templateUrl: 'driver-fab-button.html',
})
export class DriverFabButtonComponent {
  @Input() public hasContinue?: boolean;

  constructor(private userService: AuthProvider, public router: Router) {}

  public logout() {
    this.userService.logout();
    this.router.navigateByUrl('login');
  }
}
