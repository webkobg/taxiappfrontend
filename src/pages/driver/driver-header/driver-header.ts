import { Component, OnInit, Input } from '@angular/core';
import { DriverProvider } from '../../../providers/driver/driver';
import { Driver } from '../../../models/driver';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'driver-header',
  templateUrl: 'driver-header.html',
})
export class DriverHeaderComponent implements OnInit {
  @Input() public headerText: string;
  @Input() public showBackButton: boolean = true;

  public driver: Driver = new Driver();

  constructor(
    private readonly driverService: DriverProvider,
    private storage: Storage,
    private router: Router
  ) {}

  public ngOnInit(): void {
    this.driverService.driverObserver.subscribe(data => {
      if (data) {
        this.driver = data;
      }
    });
  }

  public toggleStatus(driver: Driver): void {
    this.driverService.updateDriverStatus(driver).subscribe(response => {
      const driverStorage = this.driverService.driverData;
      driverStorage.available = response.available;
      this.storage.set('driver', driverStorage);
    });
  }

  public goBack(): void {
    if (this.router.url === '/driver-order') {
      this.router.navigateByUrl('driver');
    } else if (this.router.url.includes('/driver-order/details')) {
      this.router.navigateByUrl('/driver-order');
    }
  }
}
