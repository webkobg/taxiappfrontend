import { Component, OnInit } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { TaxiorderProvider } from '../../../../providers/taxiorder/taxiorder';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TaxiOrder, OrderStatus } from '../../../../models/taxiorder';

@IonicPage()
@Component({
  selector: 'page-order-details',
  templateUrl: 'order-details.html',
})
export class OrderDetailsPage implements OnInit {
  public taxiOrders$: Observable<TaxiOrder[]>;
  public status: string;

  constructor(
    private taxiOrderService: TaxiorderProvider,
    private route: ActivatedRoute
  ) {}

  public ngOnInit(): void {
    this.route.params.subscribe(param => {
      this.status = param.status;
      this.taxiOrders$ = this.taxiOrderService.getTaxiOrdersByStatus(
        param.status
      );
    });
  }

  public trackByFn(index: number, item: number): number {
    return index;
  }

  public acceptOrder(taxiOrder: TaxiOrder, status: string): void {
    taxiOrder.status = OrderStatus[status];
    this.taxiOrderService
      .updateTaxiOrderStatus(taxiOrder, taxiOrder.userId)
      .subscribe(() => {
        this.taxiOrders$ = this.taxiOrderService.getTaxiOrdersByStatus(
          this.status
        );
      });
  }

  public getTitle(): string {
    switch (this.status.toUpperCase()) {
      case OrderStatus.Pending.toUpperCase():
        return 'Изчакващи поръчки';
      case OrderStatus.Accepted.toUpperCase():
        return 'Приети поръчки';
      case OrderStatus.Canceled.toUpperCase():
        return 'Отказани поръчки';
      case OrderStatus.Completed.toUpperCase():
        return 'Приключили поръчки';
    }
  }
}
