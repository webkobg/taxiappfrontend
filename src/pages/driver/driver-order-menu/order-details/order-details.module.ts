import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderDetailsPage } from './order-details';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../../../components/components.module';

@NgModule({
  declarations: [OrderDetailsPage],
  imports: [
    IonicPageModule.forChild(OrderDetailsPage),
    RouterModule,
    ComponentsModule,
  ],
})
export class OrderDetailsPageModule {}
