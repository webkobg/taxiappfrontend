import { Component, OnDestroy } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { TaxiOrder, OrderStatus } from '../../../models/taxiorder';
import { Observable, Subject } from 'rxjs';
import { TaxiorderProvider } from '../../../providers/taxiorder/taxiorder';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-driver-order-menu',
  templateUrl: 'driver-order-menu.html',
})
export class DriverOrderMenuPage implements OnDestroy {
  public taxiOrders$: Observable<TaxiOrder[]>;
  private destroyed$ = new Subject<any>();

  constructor(
    private readonly taxiOrderService: TaxiorderProvider,
    private router: Router
  ) {
    this.router.events
      .pipe(
        filter(
          (event: RouterEvent) =>
            event instanceof NavigationEnd && event.url === '/driver-order'
        ),
        takeUntil(this.destroyed$)
      )
      .subscribe(() => {
        this.taxiOrders$ = this.taxiOrderService.getTaxiOrders();
      });
  }

  public getCountByStatus(taxiOrders: TaxiOrder[], status: string): number {
    return taxiOrders.filter(f => f.status === OrderStatus[status]).length;
  }

  public ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
