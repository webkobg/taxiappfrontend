import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DriverOrderMenuPage } from './driver-order-menu';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [DriverOrderMenuPage],
  imports: [
    IonicPageModule.forChild(DriverOrderMenuPage),
    RouterModule,
    ComponentsModule,
  ],
})
export class DriverOrderMenuPageModule {}
