import { AgmCoreModule } from '@agm/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { JWT_OPTIONS, JwtModule } from '@auth0/angular-jwt';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Geolocation } from '@ionic-native/geolocation';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { AgmDirectionModule } from 'agm-direction';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ComponentsModule } from '../components/components.module';
import { HttpConfigInterceptor } from '../interceptor/http.interceptor';
import { DriverHomePage } from '../pages/driver/driver-home/driver-home';
import { HomePage } from '../pages/client/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPageModule } from '../pages/register/register.module';
import { SliderPage } from '../pages/slider/slider';
import { AlertProvider } from '../providers/alert/alert';
import { AuthProvider } from '../providers/auth/auth';
import { RoleGuard } from '../providers/guard/auth.guard';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { PermissionProvider } from '../providers/permission/permission';
import { PusherProvider } from '../providers/pusher/pusher';
import { TaxiorderProvider } from '../providers/taxiorder/taxiorder';
import { ROUTING } from './app-routing';
import { MyApp } from './app.component';
import { Push } from '@ionic-native/push';
import { DriverOrderMenuPageModule } from '../pages/driver/driver-order-menu/driver-order-menu.module';
import { OrderDetailsPageModule } from '../pages/driver/driver-order-menu/order-details/order-details.module';
import { RegisterPage } from '../pages/register/register';
import { DriverProvider } from '../providers/driver/driver';
import { Camera } from '@ionic-native/Camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { MinuteSecondsPipe } from '../helpers/pipes/minute-seconds.pipe';
import { ChatPageModule } from '../pages/chat/chat.module';
import { ChatPage } from '../pages/chat/chat';
import { ChatProvider } from '../providers/chat/chat';

export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => storage.get('access_token'),
    whitelistedDomains: ['localhost:3000'],
  };
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    DriverHomePage,
    SliderPage,
    RegisterPage,
    MinuteSecondsPipe,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    LeafletModule.forRoot(),
    HttpClientModule,
    ComponentsModule,
    AgmDirectionModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCFLqp7dGAeehxj73Ptd5TbTVDRxeIVybU',
      libraries: ['geometry', 'places'],
      language: 'bg',
    }),
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Storage],
      },
    }),
    IonicStorageModule.forRoot(),
    ROUTING,
    RegisterPageModule,
    DriverOrderMenuPageModule,
    OrderDetailsPageModule,
    ChatPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, HomePage, LoginPage],
  providers: [
    Camera,
    FileTransfer,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
    Geolocation,
    BackgroundGeolocation,
    LocationTrackerProvider,
    LocationAccuracy,
    AuthProvider,
    AndroidPermissions,
    RoleGuard,
    TaxiorderProvider,
    LocalNotifications,
    PusherProvider,
    AlertProvider,
    PermissionProvider,
    Push,
    DriverProvider,
    ChatProvider,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
