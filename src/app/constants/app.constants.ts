export abstract class AppConstant {
  public static TAXI_ORDER_PUSH_CHANNEL: string = 'taxiorder_data';
  public static TAXI_LOCATION_PUSH_CHANNEL: string = 'taxiorder_location';
  public static DRIVER_MARKER_ICON: string =
    'https://downloadpng.com/wp-content/uploads/thenext-thumb-cache//car-png-icon-c6b4f4d3eb48cc3e1431e0e1fbebeb6d-900x0.png';
  public static STORAGE_USER_ID: string = 'userId';
  public static STORAGE_USER_DRIVER = 'driver';
  public static PRICE_PER_KM: number = 1.26;
  public static IMAGES_BASE: string = '/images/';
  public static USER_TYPE = {
    client: 'client',
    driver: 'driver',
  };
}
