export class AppMessage {
  public static TAXI_ORDERED_TITLE_HOME: string = 'Таксито е поръчано!';
  public static TAXI_ORDERED_MESSAGE_HOME: string =
    'Поръчката е приета. Таксито ще бъде при вас за ';
  public static TAXI_CANCELED_TITLE_HOME: string = 'Таксито е отказано!';
  public static TAXI_CANCELED_MESSAGE_HOME: string = 'Поръчайте пак';
  public static TAXI_ARRIVED_TITLE_HOME: string = 'Таксито е пристигнало!';
  public static TAXI_ARRIVED_MESSAGE_HOME: string = 'Качвайте се!';
  public static TAXI_REQUEST_LOADING_MESSAGE: string =
    'Търсим шофьор, моля изчакайте!';
}
