import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';
import jwtDecode from 'jwt-decode';
import { User } from '../models/user';
import { AuthProvider } from '../providers/auth/auth';
import { AppConstant } from './constants/app.constants';

@Component({
  selector: 'app-root',
  templateUrl: 'app.html',
})
export class MyApp {
  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private authProvider: AuthProvider,
    private router: Router
  ) {
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.statusBar.styleDefault();

      this.authProvider.checkLogin();
      this.authProvider.authUser.subscribe(jwt => {
        if (!jwt) {
          this.router.navigateByUrl('/login');
          return;
        }

        const user = jwtDecode(jwt) as User;
        this.authProvider.userObserver.next(user.id);

        if (user.type === AppConstant.USER_TYPE.driver) {
          this.router.navigateByUrl('/driver');
        } else {
          this.router.navigateByUrl('/home');
        }
      });
    });
  }
}
