import { ModuleWithProviders } from '@angular/compiler/src/core';
import { RouterModule, Routes } from '@angular/router';
import { DriverHomePage } from '../pages/driver/driver-home/driver-home';
import { HomePage } from '../pages/client/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { SliderPage } from '../pages/slider/slider';
import { RoleGuard } from '../providers/guard/auth.guard';
import { DriverOrderMenuPage } from '../pages/driver/driver-order-menu/driver-order-menu';
import { OrderDetailsPage } from '../pages/driver/driver-order-menu/order-details/order-details';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginPage },
  { path: 'slider', component: SliderPage },
  { path: 'register', component: RegisterPage },
  {
    canActivate: [RoleGuard],
    component: HomePage,
    data: { expectedRole: 'client' },
    path: 'home',
  },
  {
    canActivate: [RoleGuard],
    component: DriverHomePage,
    data: { expectedRole: 'driver' },
    path: 'driver',
  },
  {
    canActivate: [RoleGuard],
    component: DriverOrderMenuPage,
    data: { expectedRole: 'driver' },
    path: 'driver-order',
    runGuardsAndResolvers: 'paramsOrQueryParamsChange',
    children: [
      {
        path: 'details/:status',
        component: OrderDetailsPage,
      },
    ],
  },
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(routes, {
  useHash: true,
});
