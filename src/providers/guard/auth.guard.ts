import { Injectable } from '@angular/core'
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router'
import jwtDecode from 'jwt-decode'
import { AuthProvider } from '../auth/auth'
@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private auth: AuthProvider, private router: Router) {}
  public canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole = route.data.expectedRole
    const token = this.auth.getToken()

    if (token) {
      const tokenPayload = jwtDecode(token)
      if (!this.auth.isAuthenticated() || tokenPayload.type !== expectedRole) {
        this.router.navigate(['login'])
        return false
      }
      return true
    } else {
      return false
    }
  }
}
