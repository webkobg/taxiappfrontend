import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TaxiOrder } from '../../models/taxiorder';
import { Observable } from 'rxjs';
import { SERVER_URL } from '../../config/config';
import { AuthProvider } from '../auth/auth';

const params = (orderStatus: string, userId: string) =>
  orderStatus === 'pending'
    ? { params: new HttpParams().set('orderStatus', orderStatus) }
    : {
        params: new HttpParams()
          .set('orderStatus', orderStatus)
          .set('userId', userId),
      };

@Injectable()
export class TaxiorderProvider {
  constructor(private http: HttpClient, private userService: AuthProvider) {}

  public addTaxiOrder(taxiOrder: TaxiOrder): Observable<TaxiOrder> {
    taxiOrder.userId = this.userService.userId;
    return this.http.post<TaxiOrder>(`${SERVER_URL}/taxiorder`, taxiOrder);
  }

  public updateTaxiOrderStatus(
    taxiOrder: TaxiOrder,
    clientId: string,
    driverLocation?: string
  ): Observable<TaxiOrder> {
    const options = {
      taxiOrderId: taxiOrder.id,
      clientId,
      driverLocation,
    };

    taxiOrder.userId = this.userService.userId;
    return this.http.put<TaxiOrder>(
      `${SERVER_URL}/taxiorder/update`,
      taxiOrder,
      { params: options }
    );
  }

  public getTaxiOrders(): Observable<TaxiOrder[]> {
    const parameters = {
      userId: this.userService.userId,
    };
    return this.http.get<TaxiOrder[]>(`${SERVER_URL}/taxiorder`, {
      params: parameters,
    });
  }

  public getTaxiOrdersByStatus(orderStatus: string): Observable<TaxiOrder[]> {
    return this.http.get<TaxiOrder[]>(
      `${SERVER_URL}/taxiorder/status`,
      params(orderStatus, this.userService.userId)
    );
  }

  public sendLocation(location: any): Observable<any> {
    return this.http.post(`${SERVER_URL}/taxiorder/location`, location);
  }
}
