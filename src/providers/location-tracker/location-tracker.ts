/// <reference types="@types/googlemaps" />

import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';
import { from, Observable, BehaviorSubject } from 'rxjs';
import BackgroundGeolocation from 'cordova-background-geolocation-lt';
import { SERVER_URL } from '../../config/config';
import { AuthProvider } from '../auth/auth';
import { Platform } from 'ionic-angular';
declare var google: any;
@Injectable()
export class LocationTrackerProvider {
  public latlngArray = [];
  public watch: any;
  public locationChange: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(
    public http: HttpClient,
    public zone: NgZone,
    public geolocation: Geolocation,
    private userService: AuthProvider,
    private platform: Platform
  ) {}

  public getCurrentLocation(): Observable<Geoposition> {
    return from(this.geolocation.getCurrentPosition());
  }

  public startTracking(finalAddress: string) {
    if (
      !this.platform.is('desktop') &&
      this.platform.is('mobile') &&
      this.platform.is('android') &&
      !this.platform.is('mobileweb')
    ) {
      BackgroundGeolocation.ready(
        {
          reset: true,
          logLevel: BackgroundGeolocation.LOG_LEVEL_OFF,
          desiredAccuracy: BackgroundGeolocation.DESIRED_ACCURACY_HIGH,
          distanceFilter: 10,
          url: `${SERVER_URL}/taxiorder/location`,
          autoSync: true,
          stopOnTerminate: true,
          startOnBoot: true,
          headers: {
            Authorization: `Bearer ${this.userService.getToken()}`,
          },
          params: {
            addressTo: finalAddress,
          },
        },
        () => {
          BackgroundGeolocation.start();
        }
      );
    }
  }

  public geocodeLatLng(geocoder: google.maps.Geocoder, lat, lng): Promise<any> {
    const latlng = { lat, lng };
    const promise = new Promise((resolve, reject) => {
      geocoder = new google.maps.Geocoder();

      geocoder.geocode({ location: latlng }, (results, status) => {
        if (status === google.maps.GeocoderStatus.OK && results[0]) {
          const firstPlace = new google.maps.LatLng(lat, lng);
          resolve({
            addressFrom: results[0].formatted_address,
            firstPlace,
          });
        } else {
          reject();
        }
      });
    });

    return promise;
  }

  public getMapStyle() {
    return this.http.get('assets/json/map-style.json');
  }
}
