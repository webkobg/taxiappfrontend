import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Driver } from '../../models/driver';
import { Observable, BehaviorSubject } from 'rxjs';
import { SERVER_URL } from '../../config/config';
import { Storage } from '@ionic/storage';

@Injectable()
export class DriverProvider {
  public driverObserver: BehaviorSubject<Driver> = new BehaviorSubject<Driver>(
    null
  );

  constructor(public http: HttpClient, private storage: Storage) {
    this.getDriverId();
  }

  public updateDriverStatus(driver: Driver): Observable<Driver> {
    return this.http.put<Driver>(`${SERVER_URL}/driver/available`, driver);
  }

  public get driverId(): string {
    return this.driverObserver.value.id;
  }

  public get driverData(): Driver {
    return this.driverObserver.value;
  }

  public getDriverId(): void {
    this.storage.get('driver').then((data: Driver) => {
      this.driverObserver.next(data);
    });
  }
}
