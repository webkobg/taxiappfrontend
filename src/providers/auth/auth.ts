import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SERVER_URL } from '../../config/config';
import { User } from '../../models/user';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export interface IJwtPaylod {
  accessToken: string;
  expiresIn: number;
}

@Injectable()
export class AuthProvider {
  public userObserver: BehaviorSubject<string> = new BehaviorSubject<string>(
    null
  );
  public authUser = new BehaviorSubject(null);

  private jwtTokenName = 'access_token';

  constructor(
    public http: HttpClient,
    private readonly storage: Storage,
    private readonly jwtHelper: JwtHelperService,
    private toastCtrl: ToastController
  ) {
    this.getUserId();
  }

  public get userId(): string {
    return this.userObserver.value;
  }

  public checkLogin(): void {
    this.storage.get(this.jwtTokenName).then(jwt => {
      if (jwt && !this.jwtHelper.isTokenExpired(jwt)) {
        this.authUser.next(jwt);
      } else {
        this.storage
          .remove(this.jwtTokenName)
          .then(() => this.authUser.next(null));
      }
    });
  }

  public getUserId(): void {
    this.storage.get('userId').then(id => {
      this.userObserver.next(id);
    });
  }

  public isAuthenticated(): boolean {
    return (
      !this.jwtHelper.isTokenExpired(this.authUser.value) && this.authUser.value
    );
  }

  public getToken(): string {
    return this.authUser.value;
  }

  public login(data: User): Observable<User> {
    return this.http.post<User>(`${SERVER_URL}/auth/token`, data).pipe(
      tap(jwt => {
        this.handleJwtResponse(jwt);
      })
    );
  }

  public async logout(): Promise<void> {
    return await this.storage.clear();
  }

  public register(user: User): Observable<User> {
    return this.http.post<User>(`${SERVER_URL}/user`, user);
  }

  public async handleError(error: HttpErrorResponse) {
    const message = `${error.status} ${error.error.message}`;

    const toast = this.toastCtrl.create({
      duration: 5000,
      message,
      position: 'bottom',
    });

    return await toast.present();
  }

  private handleJwtResponse(jwt: IJwtPaylod) {
    return this.storage
      .set(this.jwtTokenName, jwt.accessToken)
      .then(() => this.authUser.next(jwt.accessToken))
      .then(() => jwt);
  }
}
