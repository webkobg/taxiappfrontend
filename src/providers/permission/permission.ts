import { Injectable } from '@angular/core';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { throwError } from 'rxjs';

@Injectable()
export class PermissionProvider {
  constructor(
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions
  ) {}

  public checkGPSPermission(): void {
    this.androidPermissions
      .checkPermission(
        this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
      )
      .then(
        result => {
          if (result.hasPermission) {
            this.askToTurnOnGPS();
          } else {
            this.requestGPSPermission();
          }
        },
        err => {
          throwError(err);
        }
      );
  }

  private requestGPSPermission(): void {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (!canRequest) {
        this.androidPermissions
          .requestPermission(
            this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
          )
          .then(
            () => {
              this.askToTurnOnGPS();
            },
            error => {
              return throwError(error);
            }
          );
      }
    });
  }

  private askToTurnOnGPS(): void {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
      .then(error => throwError(error));
  }
}
