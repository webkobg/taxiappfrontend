import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import Pusher from 'pusher-js'

/*
  Generated class for the PusherProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PusherProvider {
  public channel: any

  constructor(public http: HttpClient) {
    const pusher = new Pusher('1f420441a89d17034c56', {
      cluster: 'eu',
    })
    this.channel = pusher.subscribe('taxiorder')
  }

  public init() {
    return this.channel
  }
}
