import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SERVER_URL } from '../../config/config';

export class Message {
  public text: string;
  public from: string;
  public to: string;
  public type: string;
}

@Injectable()
export class ChatProvider {
  constructor(public http: HttpClient) {}

  public sendMessage(message: Message): Observable<any> {
    return this.http.post(`${SERVER_URL}/taxiorder/message`, message);
  }
}
