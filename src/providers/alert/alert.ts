import { Injectable } from '@angular/core';
import { AlertController, LoadingController, Loading } from 'ionic-angular';
import { BehaviorSubject } from 'rxjs';

/*
  Generated class for the AlertProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AlertProvider {
  public acceptSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public loading: Loading;

  constructor(
    public alertController: AlertController,
    private loadingController: LoadingController
  ) {}

  public async presentAlert(message: string, title: string): Promise<void> {
    const alert = await this.alertController.create({
      title,
      message,
      buttons: [
        {
          text: 'Откажи',
          role: 'cancel',
        },
        {
          text: 'Продължи',
          role: 'ok',
          handler: () => {
            this.acceptSubject.next(true);
          },
        },
      ],
    });

    await alert.present();
  }

  public async showLoading(content: string, spinner: string) {
    this.loading = await this.loadingController.create({
      content,
      spinner,
      enableBackdropDismiss: true,
      showBackdrop: true,
    });
    await this.loading.present();
  }

  public hideLoading(): void {
    this.loading.dismiss();
  }
}
