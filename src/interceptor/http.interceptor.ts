import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthProvider } from '../providers/auth/auth';
import { Router } from '@angular/router';
import { AlertProvider } from '../providers/alert/alert';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(
    private userService: AuthProvider,
    private router: Router,
    private alertService: AlertProvider
  ) {}

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const token: string = this.userService.getToken();

    if (token) {
      request = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + token),
      });
    }

    if (!request.headers.has('Content-Type')) {
      request = request.clone({
        headers: request.headers.set('Content-Type', 'application/json'),
      });
    }

    request = request.clone({
      headers: request.headers.set('Accept', 'application/json'),
    });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        this.userService.handleError(error);
        if (error.status === 401) {
          this.userService.logout();
          this.router.navigateByUrl('login');
        }
        this.alertService.presentAlert(
          `${error.status} ${error.statusText}`,
          error.name
        );
        return throwError(error);
      })
    );
  }
}
